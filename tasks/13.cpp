#include <cassert>
#include <iostream>
#include <queue>
#include <vector>
#include <functional>

using std::size_t;
struct IGraph {
  virtual ~IGraph() = default;

  virtual void AddEdge(size_t from, size_t to) = 0;

  [[nodiscard]] virtual size_t VerticesCount() const = 0;

  [[nodiscard]] virtual std::vector<size_t> GetNextVertices(size_t vertex) const = 0;

  [[nodiscard]] virtual std::vector<size_t> GetPrevVertices(size_t vertex) const = 0;
};

class ListGraph : public IGraph {
 public:
  explicit ListGraph(size_t vertexNumber);

  explicit ListGraph(const IGraph &_graph);

  void AddEdge(size_t from, size_t to) final;

  [[nodiscard]] size_t VerticesCount() const final;

  [[nodiscard]] std::vector<size_t> GetNextVertices(size_t vertex) const final;

  [[nodiscard]] std::vector<size_t> GetPrevVertices(size_t inputVertex) const final;

 private:
  std::vector<std::vector<size_t>> lists;
};

ListGraph::ListGraph(size_t vertexCount) : lists(vertexCount) {}

ListGraph::ListGraph(const IGraph &_graph) : lists(_graph.VerticesCount()) {
  for (size_t i{0}; i < lists.size(); i++) {
	lists[i] = _graph.GetNextVertices(i);
  }
}

void ListGraph::AddEdge(size_t from, size_t to) {
  assert(from >= 0 && from < lists.size());
  assert(to >= 0 && to < lists.size());
  lists[from].push_back(to);
}

size_t ListGraph::VerticesCount() const { return lists.size(); }

std::vector<size_t> ListGraph::GetNextVertices(size_t vertex) const {
  assert(vertex >= 0 && vertex < lists.size());
  return lists[vertex];
}

std::vector<size_t> ListGraph::GetPrevVertices(size_t inputVertex) const {
  assert(inputVertex < lists.size());
  std::vector<size_t> prev_vertices;
  for (size_t i{0}; i < lists.size(); i++) {
	for (size_t j{0}; j < lists[i].size(); j++) {
	  if (inputVertex == lists[i][j]) {
		prev_vertices.push_back(i);
	  }
	}
  }
  return prev_vertices;
}

void BFS(const IGraph &graph, size_t vertex, const std::function<void(size_t)> &visit, std::vector<bool> &visited) {
  std::queue<size_t> bfsQueue;
  bfsQueue.push(vertex);
  visited[vertex] = true;
  while (!bfsQueue.empty()) {
	size_t current = bfsQueue.front();
	bfsQueue.pop();
	visit(current);
	std::vector<size_t> list = graph.GetNextVertices(current);
	for (size_t i{0}; i < list.size(); ++i) {
	  if (!visited[list[i]]) {
		bfsQueue.push(list[i]);
		visited[list[i]] = true;
	  }
	}
  }
}

size_t countComponents(const IGraph &graph) {
  size_t count{0};
  std::vector<bool> visited(graph.VerticesCount(), false);
  for (size_t i{0}; i < graph.VerticesCount(); i++) {
	if (!visited[i]) {
	  BFS(graph, i, [](const size_t &) {/*dummy func*/}, visited);
	  count++;
	}
  }
  return count;
}

void run(std::istream &input = std::cin, std::ostream &output = std::cout) {
  size_t v{0}, n{0};
  input >> v >> n;
  ListGraph graph(v);
  size_t u{0}, w{0};
  for (size_t i{0}; i < n; ++i) {
	input >> u >> w;
	graph.AddEdge(u, w);
  }
  output << countComponents(graph);
}

int main() {
  run();
  return 0;
}