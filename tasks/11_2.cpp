/*
Дано число N и N строк.
 Каждая строка содержащит команду добавления или удаления натуральных чисел,
 а также запрос на получение k-ой порядковой статистики.
 Команда добавления числа A задается положительным числом A,
 команда удаления числа A задается отрицательным числом “-A”.
 Запрос на получение k-ой порядковой статистики задается числом k. Требуемая скорость выполнения запроса - O(log n).
 */

#include <cassert>
#include <cmath>
#include <iostream>
#include <stack>

template<class T>
class AVL_tree {
private:
    struct Node {
        T key;
        unsigned char height{1};
        Node *left{nullptr};
        Node *right{nullptr};
        size_t numberOfNodes{1};
        explicit Node(T k) : key(k),
                             height(1),
                             left(nullptr),
                             right(nullptr),
                             numberOfNodes(1) {}
    };

    Node *root{nullptr};
    unsigned char height(Node *p) { return p ? p->height : 0; }
    unsigned int numberOfNodes(Node *p) { return p ? p->numberOfNodes : 0; }
    int bfactor(Node *p) { return height(p->right) - height(p->left); }

    void fixHeight(Node *p) {
        unsigned char hl = height(p->left);
        unsigned char hr = height(p->right);
        p->height = (hl > hr ? hl : hr) + 1;
    }

    void fixNumberOfNodes(Node *p) {
        unsigned int nl = numberOfNodes(p->left);
        unsigned int nr = numberOfNodes(p->right);
        p->numberOfNodes = nl + nr + 1;
    }

    Node *rotateright(Node *p) {
        Node *q = p->left;
        p->left = q->right;
        q->right = p;
        fixHeight(p);
        fixHeight(q);
        fixNumberOfNodes(p);
        fixNumberOfNodes(q);
        return q;
    }

    Node *rotateleft(Node *q) {
        Node *p = q->right;
        q->right = p->left;
        p->left = q;
        fixHeight(q);
        fixHeight(p);
        fixNumberOfNodes(q);
        fixNumberOfNodes(p);
        return p;
    }

    Node *balance(Node *p) {
        fixHeight(p);
        fixNumberOfNodes(p);
        if (bfactor(p) == 2) {
            if (bfactor(p->right) < 0)
                p->right = rotateright(p->right);
            return rotateleft(p);
        }
        if (bfactor(p) == -2) {
            if (bfactor(p->left) > 0)
                p->left = rotateleft(p->left);
            return rotateright(p);
        }
        return p;
    }

    Node *findmin(Node *parent_previous) {
        parent_previous->numberOfNodes--;
        auto parent_current = parent_previous->left;
        auto child_current = parent_current->left;
        return (child_current != nullptr) ? findmin(parent_current) : parent_previous;
    }

    Node *insert(Node *p, T k) {
        if (!p) {
            return new Node(k);
        }
        p->numberOfNodes++;
        if (k < p->key) {
            p->left = insert(p->left, k);
        } else {
            p->right = insert(p->right, k);
        }
        return balance(p);
    }

    Node *remove(Node *p, T k) {
        if (p == nullptr) {
            return nullptr;
        }
        p->numberOfNodes--;
        if (k < p->key) {
            p->left = remove(p->left, k);
        } else if (k > p->key) {
            p->right = remove(p->right, k);
        } else {
            Node *q = p->left;
            Node *r = p->right;
            delete p;
            if (r == nullptr) {
                return q;
            }
            Node *min{nullptr};
            if (r->left != nullptr) {
                Node *parentOfMin = findmin(r);
                min = parentOfMin->left;
                parentOfMin->left = min->right;
                min->right = r;
            } else {
                min = r;
            }
            min->left = q;
            return balance(min);
        }
        return balance(p);
    }

    T find_kth(Node *r, size_t pos) {
        assert(r != nullptr);
        size_t root_position{numberOfNodes(r->left)};
        if (pos == root_position) {
            return r->key;
        }
        if (pos < root_position) {
            return find_kth(r->left, pos);
        }
        return find_kth(r->right, pos - root_position - 1);
    }

public:
    AVL_tree() = default;
    explicit AVL_tree(const AVL_tree &) = delete;
    explicit AVL_tree(const AVL_tree &&) = delete;
    AVL_tree &operator=(const AVL_tree &) = delete;
    AVL_tree &operator=(const AVL_tree &&) = delete;
    /* deleting in pre-order */
    ~AVL_tree() {
        std::stack<Node *> nodes;
        nodes.push(root);
        while (!nodes.empty()) {
            Node *tempNode{nodes.top()};
            nodes.pop();
            if (tempNode->right != nullptr) {
                nodes.push(tempNode->right);
            }
            if (tempNode->left != nullptr) {
                nodes.push(tempNode->left);
            }
            delete tempNode;
        }
    }

    T kth(size_t pos) {
        return find_kth(root, pos);
    }

    void add(T k) {
        root = insert(root, k);
    }

    void deleting(T k) {
        root = remove(root, k);
    }
};


void run(std::istream &in, std::ostream &out) {
    size_t n{0};
    in >> n;
    AVL_tree<int> tree;
    for (size_t i{0}; i < n; i++) {
        int number{0}, kth{0};
        in >> number >> kth;
        if (number >= 0) {
            tree.add(number);
        } else {
            tree.deleting(std::abs(number));
        }
        out << tree.kth(kth) << std::endl;
    }
}

int main() {
    run(std::cin, std::cout);
    return 0;
}
