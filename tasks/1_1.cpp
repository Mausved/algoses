// Copyright Vladislav Maitsvetov"
/*
1.1
Подсчитать кол-во единичных бит в входном числе , стоящих на четных позициях.
Позиции битов нумеруются с 0.
*/
#include <iostream>
/* на входе инты (4 байта)*/
#define maxDigits 32

/* функция поиска первого нулевого бита справа (нужно для функции инкремента в
 * битах) */
void findingFirstNullBit(const unsigned int* const count,
                         unsigned int* const firstNullBitPos,
                         unsigned int* const offBitsBefore) {
  while ((*count & *firstNullBitPos) != 0) {
    *firstNullBitPos <<= 1;
    *offBitsBefore <<= 1;
  }
}

/*
фукнция инкремента числа в битовом режиме
P.S думал, что нельзя использовать вообще никакие арифметические операции,
потому и создал свою функцию инкремента :)
*/
void incrementInBits(unsigned int* const amountOfOnes) {
  if ((*amountOfOnes | 1) == *amountOfOnes) {
    auto firstNullBitPos{1u};
    auto offBitsBefore{~0u};
    findingFirstNullBit(amountOfOnes, &firstNullBitPos, &offBitsBefore);
    *amountOfOnes = (firstNullBitPos > *amountOfOnes)
                        ? firstNullBitPos
                        : (*amountOfOnes | firstNullBitPos) & offBitsBefore;
  } else {
    *amountOfOnes |= 1u;
  }
}

/* подсчет единичек на четных позициях */
int countOnes(unsigned int* const number) {
  auto bitPos{1u};
  auto amountOfOnes{0u};
  for (auto i{0}; i < maxDigits; i++) {
    auto isOne = ((bitPos & *number) == bitPos); /* это единица? */
    auto isEvenPos = !(i & 1); /* это четная позиция? */
    if (isOne && isEvenPos) {
      incrementInBits(&amountOfOnes); /* увеличиваем наш счетчик */
    }
    bitPos <<= 1;
  }
  return amountOfOnes;
}

int main() {
  auto number{0u};
  std::cin >> number;
  std::cout << countOnes(&number) << std::endl;
  return 0;
}
