#include <functional>
#include <iostream>
#include <stack>
#include <vector>
struct LtIntComparator {
    bool operator()(const int &l, const int &r) const {
        return l < r;
    }
};

template<class T, class Comparator>
class Tree {
private:
    struct Node {
        Node *left{nullptr};
        Node *right{nullptr};
        T value{};
    };
    Node *root{nullptr};
    size_t size{0};
    Comparator cmp;

public:
    Tree() = default;
    explicit Tree(const Tree &tree) = delete;
    explicit Tree(const Tree &&tree) = delete;
    Tree &operator=(const Tree &tree) = delete;
    Tree &operator=(const Tree &&tree) = delete;

    /* deleted in pre-order */
    ~Tree() {
        preOrder([](Node *node) { delete node; });
    }

    void preOrder(std::function<void(Node *)> printOrDeleteNode = [](Node *node) { std::cout << node->value << ' '; }) {
        std::stack<Node *> nodes;
        nodes.push(root);
        while (!nodes.empty()) {
            Node *tempNode{nodes.top()};
            nodes.pop();
            if (tempNode->right != nullptr) {
                nodes.push(tempNode->right);
            }
            if (tempNode->left != nullptr) {
                nodes.push(tempNode->left);
            }
            printOrDeleteNode(tempNode);
        }
    }

    void push(const T &elem) {
        Node *node{root};
        Node *previous{nullptr};
        while (node != nullptr) {
            previous = node;
            if (cmp(elem, node->value)) {
                node = node->left;
            } else {
                node = node->right;
            }
        }
        node = new Node;
        size++;
        node->value = elem;
        if (previous != nullptr) {
            if (cmp(node->value, previous->value)) {
                previous->left = node;
            } else {
                previous->right = node;
            }
        } else {
            root = node;
        }
    }
};

void run(std::istream &in, std::ostream &out) {
    Tree<int, LtIntComparator> tree;
    size_t n;
    in >> n;
    for (size_t i{0}; i < n; i++) {
        int temp;
        in >> temp;
        tree.push(temp);
    }
    tree.preOrder();
}

int main() {
    run(std::cin, std::cout);
    return 0;
}