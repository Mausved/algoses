//В операционной системе Technux есть планировщик процессов.
//Каждый процесс характеризуется:

//приоритетом P
//временем, которое он уже отработал t
//временем, которое необходимо для завершения работы процесса T
//Планировщик процессов выбирает процесс с минимальным значением P * (t + 1),
//выполняет его время P и кладет обратно в очередь процессов. Если выполняется
//условие t >= T, то процесс считается завершенным и удаляется из очереди.
//Требуется посчитать кол-во переключений процессора.

//Требования:

//В качестве очереди с приоритетом нужно использовать кучу.
//Куча должна быть реализована в виде шаблонного класса.
//Решение должно поддерживать передачу функции сравнения снаружи.
//Куча должна быть динамической.
//Формат ввода
//Сначала вводится кол-во процессов. После этого процессы в формате P T

//Формат вывода
//Кол-во переключений процессора.

//Пример
//Ввод	Вывод
// 3
// 1 10
// 1 5
// 2 5
// 18

#include <cassert>
#include <iomanip>
#include <iostream>
#define memory_multuply_koeff 2
using std::endl, std::cin, std::cout;

struct Process {
  size_t P{0};
  size_t T{0};
  size_t t{0};
  size_t value() const { return P * (t + 1); }
};

struct DefaultProcessComparator__lt {
  bool operator()(const Process& l, const Process& r) const {
    return l.value() < r.value();
  }
};

template <class T, class Comparator = DefaultProcessComparator__lt>
class Heap {
 public:
  Heap(T* _buffer, size_t _size) : buffer(_buffer), size(_size), capacity(0) {
    buildHeap();
  };
  explicit Heap(Heap&& _heap) {
    this->buffer = _heap.buffer;
    this->size = _heap.size;
    this->capacity = _heap.capacity;
    _heap.buffer = nullptr;
  }
  ~Heap() { delete[] buffer; }

  const T& top() const { return buffer[0]; }

  void pop() {
    assert(!empty());
    auto result = buffer[0];
    buffer[0] = std::move(buffer[--size]);
    capacity++;
    if (!empty()) {
      siftDown(0);
    }
  };
  void push(const T&& element) {
    if (full()) {
      grow();
      return push(std::move(element));
    }
    buffer[size++] = element;
    capacity--;
    siftUp(size - 1);
  }

  size_t getSize() const { return size; }
  bool empty() const { return size == 0; }
  bool full() const { return capacity == 0; }

 private:
  T* buffer;
  size_t size;
  size_t capacity;
  Heap() = delete;
  Heap(const Heap&) = delete;
  Heap& operator=(const Heap&) = delete;
  Heap& operator=(Heap&&) = delete;
  Comparator cmp;
  void siftUp(size_t idx) {
    if (idx != 0) {
      size_t parent{(idx - 1) / 2};
      if (parent > 0) {
        if (cmp(buffer[parent], buffer[idx])) {
          return;
        }
        std::swap(buffer[idx], buffer[parent]);
        idx = parent;
      }
    }
  }
  void siftDown(size_t idx) {
    size_t left{2 * idx + 1};
    size_t right{2 * idx + 2};
    size_t smallest{idx};
    if (left < size && cmp(buffer[left], buffer[idx])) {
      smallest = left;
    } else if (right < size && cmp(buffer[right], buffer[idx])) {
      smallest = right;
    }
    if (smallest != idx) {
      std::swap(buffer[idx], buffer[smallest]);
      siftDown(smallest);
    }
  }
  void buildHeap() {
    // так как size_t при декрементировании нуля уходит в максимальное значение
    for (size_t i{size / 2 - 1}; i != std::numeric_limits<size_t>::max(); --i) {
      siftDown(i);
    }
  }
  void grow() {
    size_t newSize = size * memory_multuply_koeff;
    auto newBuffer{new T[newSize]};
    for (size_t i{0}; i < size; i++) {
      newBuffer[i] = buffer[i];
    }
    delete[] buffer;
    buffer = newBuffer;
    capacity += newSize - size;
    size = newSize;
    newBuffer = nullptr;
  }
};

class Technux {
 public:
  static size_t switchings;
  static void execProcesses(Heap<Process>&& processes) {
    switchings = 0;
    while (!processes.empty()) {
      switchings++;
      Process top = processes.top();
      processes.pop();
      top.t += top.P;
      if (top.t < top.T) {
        processes.push(std::move(top));
      }
    }
  }

 private:
  Heap<Process> processes;
  Technux() = delete;
  ~Technux() = delete;
  Technux(Heap<Process>&) = delete;
  Technux(Heap<Process>&&) = delete;
  Technux(const Technux&) = delete;
  Technux(const Technux&&) = delete;
  Technux& operator=(const Technux&) = delete;
  Technux& operator=(Technux&&) = delete;
};

size_t Technux::switchings = 0;
void run(std::istream& in, std::ostream& out) {
  size_t n{0};
  in >> n;
  auto array = new Process[n];
  for (size_t i{0}; i < n; i++) {
    size_t tempP{0}, tempT{0};
    in >> tempP >> tempT;
    Process tempProc{tempP, tempT, 0};
    array[i] = tempProc;
  }
  Technux::execProcesses(Heap<Process>(array, n));
  out << Technux::switchings;
}

int main() {
  run(cin, cout);
  return 0;
}
