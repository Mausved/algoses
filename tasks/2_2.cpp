// Copyright Vladislav Maitsvetov"
/*
2_2. Дан массив целых чисел А[0..n-1].
Известно, что на интервале [0, m] значения массива строго возрастают,
а на интервале [m, n-1] строго убывают. Найти m за O(log m).
Требования:  Время работы O(log m).
Внимание! В этой задаче сначала нужно определить диапазон
для бинарного поиска размером порядка m с помощью экспоненциального поиска,
а потом уже в нем делать бинарный поиск.
*/
#include <iostream>
int binarySearch(int begin, int end, int* array, int arraySize) {
  auto middle{0};
  auto minElement{0};
  auto nextElement{0};
  auto prevElement{0};
  while (true) {
    middle = begin + (end - begin) / 2;
    minElement = array[middle];
    nextElement =
        ((middle + 1) > arraySize - 1) ? arraySize - 1 : array[middle + 1];
    prevElement = ((middle - 1) < 0) ? 0 : array[middle - 1];
    if (minElement < prevElement) {
      end = middle;
    } else if (minElement < nextElement) {
      begin = middle;
    } else {
      return middle;
    }
  }
}

int main() {
  auto arraySize{0};
  std::cin >> arraySize;
  auto array = new int[arraySize];
  for (int i = 0; i < arraySize; i++) {
    std::cin >> array[i];
  }
  auto end{1};
  /* exponencial search of binary searching area*/
  for (; end < arraySize && array[end - 1] < array[end]; end *= 2) {
  }
  auto begin{end / 2};
  end = std::min(end, arraySize);
  std::cout << binarySearch(begin, end, array, arraySize) << std::endl;
  delete[] array;
  return 0;
}
