// 7.3 binary MSD для long long
//Дан массив неотрицательных целых 64-разрядных чисел. Количество чисел не
//больше 1000000. Отсортировать массив методом MSD по битам (бинарный
//QuickSort).

#include <cmath>
#include <iostream>
#include <sstream>

#define max_digit 64

template <typename T>
void BinaryMSD(T* array, size_t size, unsigned long long pivot) {
  if (size > 1) {
    size_t i{0}, j{size - 1};
    while (i != j) {
      if ((array[i] & pivot) == 0 && i != j) {
        i++;
      }
      if ((array[j] & pivot) == pivot && i != j) {
        j--;
      }
      if ((array[i] & pivot) == pivot && (array[j] & pivot) == 0) {
        std::swap(array[i], array[j]);
      }
    }
    if ((array[i] & pivot) == pivot) {
      i--;
    }
    if ((array[j] & pivot) == 0) {
      j++;
    }
    pivot >>= 1;
    if (pivot != 0) {
      BinaryMSD(array, j, pivot);
      BinaryMSD(array + j, size - j, pivot);
    }
  } else {
    return;
  }
}

void run(std::istream& in, std::ostream& out) {
  size_t size{0};
  in >> size;
  auto array{new unsigned long long[size]};
  for (size_t i{0}; i < size; i++) {
    in >> array[i];
  }
  auto pivot = (unsigned long long)std::pow(2, 63);
  BinaryMSD(array, size, pivot);

  for (size_t i{0}; i < size; i++) {
    out << array[i] << ' ';
  }
  out << std::endl;
  delete[] array;
  array = nullptr;
}

int main() {
  run(std::cin, std::cout);
  return 0;
}
