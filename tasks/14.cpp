/**
 * Задача 3. «Города» (4 балла) Требуется отыскать самый выгодный маршрут между городами.
 * Требования: время работы O((N+M)logN), где N-количество городов, M-известных дорог между ними.
 * Формат входных данных.
 * Первая строка содержит число N – количество городов.
 * Вторая строка содержит число M - количество дорог.
 * Каждая следующая строка содержит описание дороги(откуда, куда, время в пути).
 * Последняя строка содержит маршрут (откуда и куда нужно доехать).
 * Формат выходных данных.
 * Вывести длину самого выгодного маршрута.
 */
#include <cassert>
#include <iostream>
#include <limits>
#include <vector>
#include <set>

using std::size_t;
class ListGraph {
 public:
  explicit ListGraph(const size_t &vertexNumber);

  void AddEdge(size_t from, size_t to, size_t weight);
  [[nodiscard]] size_t VerticesCount() const;
  [[nodiscard]] std::vector<std::pair<size_t, size_t>> GetNextVertices(size_t vertex) const;

 private:
  std::vector<std::vector<std::pair<size_t, size_t>>> lists;
};

ListGraph::ListGraph(const size_t &vertexNumber) : lists(vertexNumber) {}

void ListGraph::AddEdge(size_t from, size_t to, size_t weight) {
  assert(from >= 0 && from < lists.size());
  assert(to >= 0 && to < lists.size());
  lists[from].push_back(std::make_pair(weight, to));
  lists[to].push_back(std::make_pair(weight, from));
}

size_t ListGraph::VerticesCount() const { return lists.size(); }

std::vector<std::pair<size_t, size_t>> ListGraph::GetNextVertices(size_t vertex) const {
  assert(vertex >= 0 && vertex < lists.size());
  return lists[vertex];
}

size_t Dijkstra(const ListGraph &graph, size_t start, size_t finish) {
  std::vector<size_t> distance(graph.VerticesCount(), std::numeric_limits<size_t>::max());
  distance[start] = 0;
  std::set<std::pair<size_t, size_t>> setPairs;
  setPairs.insert(std::make_pair(0, start));
  while (!setPairs.empty()) {
	auto current_vertex = *setPairs.begin();
	setPairs.erase(setPairs.begin());
	if (distance[current_vertex.second] < current_vertex.first) {
	  continue;
	}
	for (std::pair<size_t, size_t> next : graph.GetNextVertices(current_vertex.second)) {
	  if (distance[next.second] == std::numeric_limits<size_t>::max()) {
		distance[next.second] = distance[current_vertex.second] + next.first;
		setPairs.insert(std::make_pair(next.first, next.second));
	  } else if (distance[current_vertex.second] + next.first < distance[next.second]) {
		setPairs.erase(std::make_pair(distance[next.second], next.second));
		distance[next.second] = distance[current_vertex.second] + next.first;
		setPairs.insert(std::make_pair(distance[next.second], next.second));
	  }
	}
  }
  return distance[finish];
}

void run(std::istream &input = std::cin, std::ostream &output = std::cout) {
  size_t n{0}, m{0};
  input >> n >> m;
  ListGraph graph(n);
  for (size_t i{0}; i < m; ++i) {
	size_t from{0}, to{0}, weight{0};
	input >> from >> to >> weight;
	graph.AddEdge(from, to, weight);
  }
  size_t start{0}, end{0};
  input >> start >> end;
  output << Dijkstra(graph, start, end);
}

int main() {
  run();
  return 0;
}