// 5_4
//На числовой прямой окрасили N отрезков.
//Известны координаты левого и правого концов каждого отрезка [Li, Ri]. Найти
//сумму длин частей числовой прямой, окрашенных ровно в один слой. N ≤ 10000.
// Li, Ri — целые числа в диапазоне [0, 109].

//Формат ввода
//В первой строке записано количество отрезков.
//В каждой последующей строке через пробел записаны координаты левого и правого
//концов отрезка.

//Формат вывода
//Выведите целое число — длину окрашенной в один слой части.

//Пример
//Ввод	Вывод
// 3
// 1 4
// 7 8
// 2 5
// 3

#include <cstring>
#include <iostream>
using std::endl;

struct point {
  size_t coord{0};
  bool isBegin{true};
  int operator-(const point& p) const { return (this->coord - p.coord); }
};

struct PointComparator {
  bool operator()(const point& l, const point& r) const {
    return l.coord < r.coord;
  }
};

std::ostream& operator<<(std::ostream& out, const point& p) {
  out << p.coord << ' ';
  return out;
}

template <typename T, typename Comparator = PointComparator>
void Merge(T* arrayLeft, size_t arrayLeftSize, T* arrayRight,
           size_t arrayRightSize, T* buffer, Comparator cmp) {
  size_t i{0}, j{0};
  while (i != arrayLeftSize || j != arrayRightSize) {
    size_t posInBuffer = i + j;
    if (i == arrayLeftSize && j != arrayRightSize) {
      buffer[posInBuffer] = arrayRight[j++];
    } else if (j == arrayRightSize && i != arrayLeftSize) {
      buffer[posInBuffer] = arrayLeft[i++];
    } else if (cmp(arrayRight[j], arrayLeft[i])) {
      buffer[posInBuffer] = arrayRight[j++];
    } else {
      buffer[posInBuffer] = arrayLeft[i++];
    }
  }
}

template <typename T, typename DefaultComparator = PointComparator>
void MergeSort(T* array, size_t size, DefaultComparator cmp) {
  if (size <= 1) {
    return;
  }
  size_t firstLen = size / 2;
  size_t secondLen = size - firstLen;
  MergeSort(array, firstLen, cmp);
  MergeSort(array + firstLen, secondLen, cmp);
  auto tempBuffer{new T[size]};
  Merge(array, firstLen, array + firstLen, secondLen, tempBuffer, cmp);
  for (size_t i{0}; i < size; i++) {
    array[i] = tempBuffer[i];
  }
  delete[] tempBuffer;
  tempBuffer = nullptr;
}

void run(std::istream& in, std::ostream& out) {
  size_t n{0};
  in >> n;
  size_t size{2 * n};
  auto array{new point[size]};
  for (size_t i{0}; i < size - 1;) {
    point begin{0, true}, end{0, false};
    in >> begin.coord >> end.coord;
    array[i++] = begin;
    array[i++] = end;
  }
  PointComparator cmp;
  MergeSort(array, size, cmp);
  size_t thickness{0};
  size_t len{0};
  for (size_t i{0}; i < size; i++) {
    if (i != 0 && thickness == 1) {
      len += array[i] - array[i - 1];
    }
    if (array[i].isBegin) {
      thickness++;
    } else {
      thickness--;
    }
  }
  out << len << endl;
  delete[] array;
}

int main() {
  run(std::cin, std::cout);
  return 0;
}
