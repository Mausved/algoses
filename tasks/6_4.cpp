// 6_4. Реализуйте стратегию выбора опорного элемента “случайный элемент”.
// Функцию Partition реализуйте методом прохода двумя итераторами от конца
// массива к началу.

#include <cstdlib>
#include <iostream>
#include <sstream>
using std::endl;

template <typename T>
struct DefaultComparator {
  bool operator()(const T& l, const T& r) const { return (l > r); }
};

template <typename T, typename Comparator = DefaultComparator<T>>
size_t partition(T* array, size_t l, size_t r, Comparator cmp) {
  //случайный элемент в диапазоне (l; r);
  size_t pivot_pos = l + std::rand() % (r - l + 1);
  //т.к идем с конца к началу, то пивот лучше ставить в начало
  std::swap(array[pivot_pos], array[l]);
  T pivot = array[l];
  size_t i{r}, j{i};
  while (true) {
    if (cmp(array[i], pivot) && i > l && i == j) {
      i--;
      j--;
    } else if (j > l) {
      if (cmp(array[j], pivot)) {
        std::swap(array[j], array[i--]);
      }
      j--;
    }
    if (j == l) {
      std::swap(array[i], array[l]);
      return i;
    }
  }
}

template <typename T, typename Comparator = DefaultComparator<T>>
T kth_statistic(T* array, size_t k, size_t l, size_t r, Comparator cmp) {
  size_t pivot_pos{partition(array, l, r, cmp)};
  while (pivot_pos != k) {
    if (l == r) {
      return array[l];
    }
    pivot_pos = partition(array, l, r, cmp);
    if (cmp(k, pivot_pos)) {
      l = pivot_pos + 1;
    } else {
      r = pivot_pos - 1;
    }
  }
  return array[pivot_pos];
}

void run(std::istream& in, std::ostream& out) {
  size_t size{0};
  //сбрасывание рандомайзера перед каждым запуском через системные часы
  std::srand(static_cast<unsigned int>(time(0)));
  in >> size;
  auto array{new int[size]};
  for (size_t i{0}; i < size; i++) {
    in >> array[i];
  }

  DefaultComparator<int> cmp;
  auto _10percentil = kth_statistic(array, (int)(0.1 * size), 0, size - 1, cmp);
  auto _mediana = kth_statistic(array, size / 2, 0, size - 1, cmp);
  auto _90percentil = kth_statistic(array, (int)(0.9 * size), 0, size - 1, cmp);
  out << _10percentil << endl;
  out << _mediana << endl;
  out << _90percentil << endl;
  delete[] array;
  array = nullptr;
}

int main() {
  std::stringstream in;
  in << "10" << endl << "1 2 3 4 5 6 7 8 9 10" << endl;
  run(std::cin, std::cout);
  return 0;
}
