// Copyright <Vladislav Maitsvetov>

/*
Во всех задачах из следующего списка следует написать структуру данных,
обрабатывающую команды push* и pop*. Формат входных данных. В первой строке
количество команд n. n ≤ 1000000. Каждая команда задаётся как 2 целых числа: a
b. a = 1 - push front a = 2 - pop front a = 3 - push back a = 4 - pop back
Команды добавления элемента 1 и 3 заданы с неотрицательным параметром b.
Для очереди используются команды 2 и 3. Для дека используются все четыре
команды. Если дана команда pop*, то число b - ожидаемое значение. Если команда
pop вызвана для пустой структуры данных, то ожидается “-1”. Формат выходных
данных. Требуется напечатать YES - если все ожидаемые значения совпали. Иначе,
если хотя бы одно ожидание не оправдалось, то напечатать NO. 3.2 Реализовать дек
с динамическим зацикленным буфером. Требования: Дек должен быть реализован в
виде класса.
*/

#include <cassert>
#include <cstring>
#include <iostream>
#include <sstream>
#define _default_buffer_size 8
#define _default_size_multiply 2

template <typename T>
class CycledBuffer {
 public:
  CycledBuffer()
      : bufferSize(_default_buffer_size),
        bufferCapacity(bufferSize),
        buffer(new T[bufferSize]),
        head(0),
        tail(0) {}
  ~CycledBuffer<T>() { delete[] buffer; }
  bool isEmpty() const { return (bufferCapacity == bufferSize); }
  bool isFull() const { return (bufferCapacity == 0); }
  void pushBack(const T element);
  void pushFront(const T element);
  T popBack();
  T popFront();
  void printBufer(std::ostream& out = std::cout) const;
  int getSize() const { return bufferSize; }

 private:
  int bufferSize;
  int bufferCapacity;
  T* buffer;
  int head;
  int tail;
  void reallocate();
  explicit CycledBuffer(size_t _n) = delete;
  CycledBuffer(CycledBuffer&& _buffer) = delete;
  CycledBuffer(const CycledBuffer& _buffer) = delete;
  CycledBuffer& operator=(const CycledBuffer& _buffer) = delete;
  CycledBuffer& operator=(const CycledBuffer&& _buffer) = delete;
  int incrementHeadOrTail(int HeadOrTail);
  int decrementHeadOrTail(int HeadOrTail);
};

template <typename T>
class Deque {
 public:
  Deque() { cycledBufer = new CycledBuffer<T>(); }

  void pushBack(T element) { cycledBufer->pushBack(element); }

  void pushFront(T element) { cycledBufer->pushFront(element); }

  bool isEmpty() { return cycledBufer->isEmpty(); }

  T popBack() { return cycledBufer->popBack(); }

  T popFront() { return cycledBufer->popFront(); }

  void printDeque(std::ostream& out = std::cout) const {
    cycledBufer->printBufer(out);
  }

  int getSize() const { return cycledBufer->getSize(); }

  ~Deque() {
    delete cycledBufer;
    cycledBufer = nullptr;
  }

 private:
  Deque(const Deque& _deq) = delete;
  Deque(const Deque&& _deq) = delete;
  Deque& operator=(const Deque& _deq) = delete;
  Deque& operator=(const Deque&& _deq) = delete;
  CycledBuffer<T>* cycledBufer;
};

template <typename T>
int CycledBuffer<T>::incrementHeadOrTail(int HeadOrTail) {
  HeadOrTail = (HeadOrTail + 1 >= bufferSize) ? 0 : HeadOrTail + 1;
  return HeadOrTail;
}

template <typename T>
int CycledBuffer<T>::decrementHeadOrTail(int HeadOrTail) {
  HeadOrTail = (HeadOrTail - 1 < 0) ? bufferSize - 1 : HeadOrTail - 1;
  return HeadOrTail;
}

template <typename T>
void CycledBuffer<T>::pushBack(const T element) {
  if (isEmpty()) {
    buffer[tail] = element;
    bufferCapacity--;
    return;
  } else if (isFull()) {
    reallocate();
    return pushBack(element);
  } else {
    tail = incrementHeadOrTail(tail);
    buffer[tail] = element;
    bufferCapacity--;
    return;
  }
}

template <typename T>
void CycledBuffer<T>::pushFront(const T element) {
  if (isEmpty()) {
    buffer[tail] = element;
    bufferCapacity--;
    return;
  } else if (isFull()) {
    reallocate();
    return pushFront(element);
  } else {
    head = decrementHeadOrTail(head);
    buffer[head] = element;
    bufferCapacity--;
    return;
  }
}

template <typename T>
T CycledBuffer<T>::popBack() {
  if (!isEmpty()) {
    auto getted = buffer[tail];
    tail = decrementHeadOrTail(tail);
    bufferCapacity++;
    if (isEmpty()) {
      head = tail = 0;
    }
    return getted;
  }
  head = tail = 0;
  bufferCapacity = bufferSize;
  return -1;
}

template <typename T>
T CycledBuffer<T>::popFront() {
  if (!isEmpty()) {
    auto getted = buffer[head];
    head = incrementHeadOrTail(head);
    bufferCapacity++;
    if (isEmpty()) {
      head = tail = 0;
    }
    return getted;
  }
  head = tail = 0;
  bufferCapacity = bufferSize;
  return -1;
}

template <typename T>
void CycledBuffer<T>::reallocate() {
  auto newSize{bufferSize * _default_size_multiply};
  auto newBuffer = new T[newSize];
  auto tempHead{head};
  auto tempTail{tail};
  head = 0;
  tail = 0;
  auto tempPtr = buffer;
  buffer = newBuffer;
  auto tempSize = bufferSize;
  bufferSize = newSize;
  bufferCapacity = bufferSize;

  for (auto i{tempHead}; i != tempTail; i = (i + 1 == tempSize) ? 0 : i + 1) {
    pushBack(tempPtr[i]);
  }

  pushBack(tempPtr[tempTail]);
  delete[] tempPtr;
  newBuffer = nullptr;
}

template <typename T>
void CycledBuffer<T>::printBufer(std::ostream& out) const {
  out << "\n---------------\n";
  auto tempHead{head}, tempTail{tail};
  out << "head=" << head << " tail=" << tail << std::endl;
  if (head != tail) {
    for (auto i{tempHead}; i != tempTail;
         i = (i + 1 == bufferSize) ? 0 : i + 1) {
      out << buffer[i] << ' ';
    }
    out << buffer[tempTail];
  } else {
    out << buffer[head];
  }
  out << "\n---------------\n";
}

void testDequeLogic(std::ostream& out) {
  Deque<int> deq;
  for (auto i{0}; i < 4; i++) {
    deq.pushBack(i);
  }
  deq.printDeque();
  out << deq.popBack() << std::endl;
  deq.printDeque();
  deq.pushFront(10);
  deq.printDeque();
  deq.pushFront(1);
  deq.pushFront(1);
  deq.pushFront(10);
  deq.pushFront(20);
  deq.pushFront(200);
  deq.printDeque();
  for (auto i{10}; i < 24; i++) {
    deq.pushFront(i);
  }
  deq.printDeque();
  std::cout << "popBack(2): " << deq.popBack() << std::endl;
  deq.printDeque();
  std::cout << "popFronts(20 values):\n";
  for (auto i{0}; i < 23; i++) {
    std::cout << deq.popFront() << ' ';
  }
  deq.printDeque();

  deq.pushBack(1);
  out << deq.popBack() << deq.popBack() << deq.popBack() << deq.popBack()
      << std::endl;

  for (auto i{10}; i < 24; i++) {
    deq.pushFront(i);
  }
  deq.printDeque();
  out << std::endl << deq.popBack() << std::endl;
  deq.printDeque();
}

void run(std::istream& in, std::ostream& out) {
  Deque<int> deq;
  auto commands{0};
  int value{0}, get{0};
  in >> commands;
  for (int i{0}; i < commands; i++) {
    int command{0};
    in >> command >> value;
    switch (command) {
      case 1:
        deq.pushFront(value);
        break;
      case 2:
        get = deq.popFront();
        if (get != value) {
          out << "NO\n";
          std::cout << "(popfront)awaiting=" << value << " but get=" << get
                    << std::endl;
          return;
        }
        break;
      case 3:
        deq.pushBack(value);
        break;
      case 4:
        get = deq.popBack();
        if (get != value) {
          out << "NO\n";
          std::cout << "(popback)awaiting=" << value << " but get=" << get
                    << std::endl;
          return;
        }
        break;
      default:
        out << "NO\n";
        return;
    }
  }
  out << "YES\n";
  return;
}

void TEST() {
  {
    Deque<int> deq;
    std::stringstream in;
    std::stringstream out;

    in << "3" << std::endl
       << "1 44" << std::endl
       << "3 50" << std::endl
       << "2 44" << std::endl;
    run(in, out);
    assert(out.str() == "YES\n");
    std::cout << "passed#1\n";
  }

  {
    Deque<int> deq;
    std::stringstream in;
    std::stringstream out;
    in << "2" << std::endl << "2 -1" << std::endl << "3 10" << std::endl;
    run(in, out);
    assert(out.str() == "YES\n");
    std::cout << "passed#2\n";
  }

  {
    Deque<int> deq;
    std::stringstream in;
    std::stringstream out;
    in << "2" << std::endl << "3 44" << std::endl << "2 66" << std::endl;
    run(in, out);
    assert(out.str() == "NO\n");
    std::cout << "passed#3\n";
  }
  {
    Deque<int> deq;
    std::stringstream in;
    std::stringstream out;
    in << "6" << std::endl
       << "4 -1" << std::endl
       << "3 5" << std::endl
       << "4 5" << std::endl
       << "1 10" << std::endl
       << "4 10" << std::endl
       << "2 -1" << std::endl;
    run(in, out);
    assert(out.str() == "YES\n");
    std::cout << "passed#4\n";
  }

  {
    Deque<int> deq;
    std::stringstream in;
    std::stringstream out;
    in << "6" << std::endl
       << "1 10" << std::endl
       << "3 100" << std::endl
       << "1 200" << std::endl
       << "3 300" << std::endl
       << "1 400" << std::endl
       << "3 500" << std::endl;
    run(in, out);
    assert(out.str() == "YES\n");
    std::cout << "passed#5\n";
  }

  {
    Deque<int> deq;
    std::stringstream in;
    std::stringstream out;
    in << "6" << std::endl
       << "1 10" << std::endl
       << "1 100" << std::endl
       << "1 200" << std::endl
       << "1 300" << std::endl
       << "1 400" << std::endl
       << "2 400" << std::endl;
    run(in, out);
    assert(out.str() == "YES\n");
    std::cout << "passed#6\n";
  }

  {
    Deque<int> deq;
    std::stringstream in;
    std::stringstream out;
    in << "6" << std::endl
       << "1 10" << std::endl
       << "1 100" << std::endl
       << "1 200" << std::endl
       << "1 300" << std::endl
       << "1 400" << std::endl
       << "3 400" << std::endl;
    run(in, out);
    assert(out.str() == "YES\n");
    std::cout << "passed#7\n";
  }

  {
    Deque<int> deq;
    std::stringstream in;
    std::stringstream out;
    in << "22" << std::endl
       << "1 10" << std::endl
       << "1 100" << std::endl
       << "1 200" << std::endl
       << "1 300" << std::endl
       << "1 400" << std::endl
       << "4 10" << std::endl
       << "4 100" << std::endl
       << "4 200" << std::endl
       << "4 300" << std::endl
       << "4 400" << std::endl
       << "4 -1" << std::endl
       << "4 -1" << std::endl
       << "4 -1" << std::endl
       << "1 100" << std::endl
       << "4 100" << std::endl
       << "4 -1" << std::endl
       << "2 -1" << std::endl
       << "3 0" << std::endl
       << "2 0" << std::endl
       << "3 0" << std::endl
       << "2 0" << std::endl
       << "4 -1" << std::endl;
    run(in, out);
    assert(out.str() == "YES\n");
    std::cout << "passed#8\n";
  }
}

int main() {
  testDequeLogic(std::cout);
  TEST();
  // run(std::cin, std::cout);
  return 0;
}
