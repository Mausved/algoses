/*
Постройте B-дерево минимального порядка t и выведите его по слоям.
В качестве ключа используются числа, лежащие в диапазоне 0..232 -1

Требования:
    B-дерево должно быть реализовано в виде шаблонного класса.
    Решение должно поддерживать передачу функции сравнения снаружи.

    Формат входных данных
            Сначала вводится минимальный порядок дерева t.
    Затем вводятся элементы дерева.

    Формат выходных данных
Программа должна вывести B-дерево по слоям. Каждый слой на новой строке, элементы должны выводится в том порядке, в котором они лежат в узлах.

 */
#include <cassert>
#include <iostream>
#include <iterator>
#include <limits>
#include <queue>
#include <sstream>
#include <vector>

struct DefaultIntComparator_lt {
    bool operator()(const int& l, const int& r) {
        return (l < r);
    }
};
template<class T, class Comparator = DefaultIntComparator_lt>
class BTree {
public:
    explicit BTree(size_t _t) : root(nullptr),
                                t(_t) { assert(t >= 2); }
    ~BTree() { delete root; }
    void Insert(const T& key);
    void print(std::ostream& out);

private:
    struct Node {
        bool isLeaf{false};
        std::vector<T> keys;
        std::vector<Node*> children;
        Node() = default;
        explicit Node(bool isLeaf) : isLeaf(isLeaf) {}
        ~Node() {
            for (Node* child: children) { delete child; }
        }
    };

    Comparator cmp;
    Node* root;
    size_t t;

    void splitChild(Node* node, size_t pos);
    void insertInNonFullTree(Node* node, const T& key);
    bool isNodeFull(Node* node);
};

template<class T, class Comparator>
void BTree<T, Comparator>::splitChild(Node* node, size_t pos) {
    auto child = new Node(t);
    auto brother = node->children[pos];
    child->isLeaf = brother->isLeaf;
    child->keys.resize(t - 1);

    for (size_t j{0}; j < t - 1; j++) {
        child->keys[j] = brother->keys[j + t];
    }

    if (!brother->isLeaf) {
        for (size_t j{0}; j < t; j++) {
            child->children.push_back(brother->children[j + t]);
        }
        brother->children.resize(t);
    }
    brother->keys.resize(t - 1);
    node->children.resize(node->children.size() + 1);
    for (size_t j{node->children.size() - 1}; j > pos; j--) {
        node->children[j] = node->children[j - 1];
    }
    node->children[pos + 1] = child;
    node->keys.resize(node->keys.size() + 1);
    for (size_t j{node->keys.size() - 1}; j > pos && j != std::numeric_limits<size_t>::max(); j--) {
        node->keys[j] = node->keys[j - 1];
    }
    node->keys[pos] = brother->keys[t - 1];
}

template<class T, class Comparator>
void BTree<T, Comparator>::Insert(const T& key) {

    if (!root) {
        root = new Node(true);
    }
    if (isNodeFull(root)) {
        Node* newRoot = new Node(false);
        newRoot->children.push_back(root);
        root = newRoot;
        splitChild(root, 0);
    }
    insertInNonFullTree(root, key);
}

template<class T, class Comparator>
void BTree<T, Comparator>::insertInNonFullTree(Node* node, const T& key) {
    int pos = node->keys.size() - 1;
    if (node->isLeaf) {
        node->keys.resize(node->keys.size() + 1);
        while (pos >= 0 && cmp(key, node->keys[pos])) {
            node->keys[pos + 1] = node->keys[pos];
            pos--;
        }
        node->keys[pos + 1] = key;
    } else {
        while (pos >= 0 && cmp(key, node->keys[pos])) {
            --pos;
        }
        if (isNodeFull(node->children[pos + 1])) {
            splitChild(node, pos + 1);
            if (cmp(node->keys[pos + 1], key)) {
                ++pos;
            }
        }
        insertInNonFullTree(node->children[pos + 1], key);
    }
}

template<class T, class Comparator>
bool BTree<T, Comparator>::isNodeFull(Node* node) {
    return node->keys.size() == 2 * t - 1;
}

template<class T, class Comparator>
void BTree<T, Comparator>::print(std::ostream& out) {
    if (root == nullptr) {
        return;
    }
    std::queue<Node*> queue;
    queue.push(root);
    while (!queue.empty()) {
        std::queue<Node*> tempQueue;
        while (!queue.empty()) {
            Node* tempNode = queue.front();
            queue.pop();
            std::copy(tempNode->keys.begin(), tempNode->keys.end(), std::ostream_iterator<T>{out, " "});
            for (Node* child: tempNode->children) {
                tempQueue.push(child);
            }
        }
        out << std::endl;
        queue = tempQueue;
    }
}

void run(std::istream& in, std::ostream& out) {
    size_t t{0};
    in >> t;
    BTree<unsigned int> bTree(t);
    unsigned int element{0};
    while (in >> element) {
        bTree.Insert(element);
    }
    bTree.print(out);
}


int main() {
    run(std::cin, std::cout);
    return 0;
}
