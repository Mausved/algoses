/*
Реализуйте структуру данных типа “множество строк” на основе динамической
хеш-таблицы с открытой адресацией. Хранимые строки непустые и состоят из
строчных латинских букв. Хеш-функция строки должна быть реализована с помощью
вычисления значения многочлена методом Горнера. Начальный размер таблицы должен
быть равным 8-ми. Перехеширование выполняйте при добавлении элементов в случае,
когда коэффициент заполнения таблицы достигает 3/4. Структура данных должна
поддерживать операции добавления строки в множество, удаления строки из
множества и проверки принадлежности данной строки множеству. 1_1. Для разрешения
коллизий используйте квадратичное пробирование. i-ая проба g(k, i)=g(k, i-1) + i
(mod m). m - степень двойки.

*/

#include <assert.h>

#include <iostream>
#include <sstream>
#include <string>
#include <vector>

size_t constexpr multiplier = 23;
struct StringHasher {
  size_t operator()(const std::string& str) const {
    size_t hash = 0;
    for (size_t i = 0; i < str.size(); ++i) hash = hash * multiplier + str[i];
    return hash;
  }
};

template <class T, class H>
class HashTable {
 public:
  explicit HashTable(const H& _hasher);
  ~HashTable() = default;
  HashTable(const HashTable& table) = delete;
  HashTable& operator=(const HashTable& table) = delete;
  bool Has(const T& data) const;
  bool Add(const T& data);
  bool Delete(const T& data);

 private:
  struct HashTableNode {
    T Data;
    size_t Hash;
    enum State { empty, del, data } TNodeState;
    HashTableNode() : Hash(0), TNodeState(State::empty) {}
    HashTableNode(const T& data, const size_t& hash)
        : Data(data), Hash(hash), TNodeState(State::data) {}
    explicit HashTableNode(const HashTableNode& _node) {
      Data = _node.Data;
      Hash = _node.Hash;
      TNodeState = _node.TNodeState;
    }
    HashTableNode& operator=(const HashTableNode& _node) {
      this->Data = _node.Data;
      this->Hash = _node.Hash;
      this->TNodeState = _node.TNodeState;
      return *this;
    }
  };
  void rehashing(const size_t& coeff);

 private:
  H hasher;
  std::vector<HashTableNode> table;
  size_t keysCount;
  size_t delCount;
};

size_t constexpr __defaultHashTableSize = 8;
template <class T, class H>
HashTable<T, H>::HashTable(const H& _hasher)
    : hasher(_hasher),
      table(__defaultHashTableSize),
      keysCount(0),
      delCount(0) {}

template <class T, class H>
bool HashTable<T, H>::Has(const T& data) const {
  size_t previousHash = hasher(data);
  for (size_t i{0}; i < table.size(); i++) {
    size_t hash = ((i == 0) ? previousHash : (previousHash + i)) % table.size();
    auto node = &table[hash];
    if (node->TNodeState == HashTableNode::State::empty) {
      return false;
    } else if (node->TNodeState == HashTableNode::State::data) {
      if (node->Data == data) {
        return true;
      }
    }
    previousHash = hash;
  }
  return false;
}

size_t constexpr __defaultDelMultiplyCoeff = 1;
size_t constexpr __defaultGrowerMultiplyCoeff = 2;
template <class T, class H>
bool HashTable<T, H>::Add(const T& data) {
  // std::cout << "added data=" << data << std::endl;
  if (keysCount >= table.size() / 2) {
    rehashing(__defaultGrowerMultiplyCoeff);
    return Add(data);
  }

  size_t absHash{hasher(data)};
  size_t previousHash{absHash};
  HashTableNode* delNode{nullptr};
  for (size_t i{0}; i < table.size(); i++) {
    size_t hash = ((i == 0) ? previousHash : (previousHash + i)) % table.size();
    auto node = &table[hash];
    if (node->TNodeState == HashTableNode::State::empty) {
      if (delNode == nullptr) {
        *node = HashTableNode(data, absHash);
      } else {
        *delNode = HashTableNode(data, absHash);
        delCount--;
      }
      keysCount++;
      return true;
    } else if (node->TNodeState == HashTableNode::State::del) {
      delNode = (delNode == nullptr) ? &table[hash] : delNode;
    } else if (node->TNodeState == HashTableNode::State::data) {
      if (node->Data == data) {
        return false;
      }
    }
    previousHash = hash;
  }
  return false;
}

template <class T, class H>
bool HashTable<T, H>::Delete(const T& data) {
  if (delCount >= table.size() / 3) {
    rehashing(__defaultDelMultiplyCoeff);
    return Delete(data);
  }

  size_t previousHash = hasher(data);
  for (size_t i{0}; i < table.size(); i++) {
    size_t hash = ((i == 0) ? previousHash : (previousHash + i)) % table.size();
    auto node = &table[hash];
    if (node->TNodeState == HashTableNode::State::empty) {
      return false;
    } else if (node->TNodeState == HashTableNode::State::data) {
      if (node->Data == data) {
        node->TNodeState = HashTableNode::State::del;
        delCount++;
        keysCount--;
        return true;
      }
    }
    previousHash = hash;
  }
  return false;
}

template <class T, class H>
void HashTable<T, H>::rehashing(const size_t& coeff) {
  std::vector<HashTableNode> newTable(table.size() * coeff);
  for (size_t i{0}; i < table.size(); i++) {
    if (table[i].TNodeState == HashTableNode::State::data) {
      size_t newHash = table[i].Hash % newTable.size();
      size_t previousHash = newHash;

      for (size_t i{0};
           i < newTable.size() &&
           newTable[previousHash].TNodeState ==
               HashTableNode::State::data; /* не допускаем коллизий */
           i++) {
        previousHash =
            (previousHash + i) % newTable.size();  //квадратичное пробирование
      }

      newHash = previousHash;
      newTable[newHash] = table[i];
    }
  }
  table = std::move(newTable);
  delCount = 0;
}

void run(std::istream& in, std::ostream& out) {
  StringHasher hasher;
  HashTable<std::string, StringHasher> table(hasher);
  char operation = 0;
  std::string data;
  while (in >> operation >> data) {
    switch (operation) {
      case '+':
        out << (table.Add(data) ? "OK" : "FAIL") << std::endl;
        break;
      case '-':
        out << (table.Delete(data) ? "OK" : "FAIL") << std::endl;
        break;
      case '?':
        out << (table.Has(data) ? "OK" : "FAIL") << std::endl;
        break;
      default:
        assert(false);
    }
  }
}

int main() {
  run(std::cin, std::cout);
  return 0;
}
