#include "IGraph.h"
#include "queue"

void BFS(const IGraph &graph, size_t vertex, const std::function<void(size_t)> &visit) {
  std::vector<bool> visited(graph.VerticesCount(), false);
  std::queue<size_t> bfsQueue;
  bfsQueue.push(vertex);
  visited[vertex] = true;
  while (!bfsQueue.empty()) {
	size_t current = bfsQueue.front();
	bfsQueue.pop();
	visit(current);
	std::vector<size_t> list = graph.GetNextVertices(current);
	for (size_t i{0}; i < list.size(); ++i) {
	  if (!visited[list[i]]) {
		bfsQueue.push(list[i]);
		visited[list[i]] = true;
	  }
	}
  }
}
