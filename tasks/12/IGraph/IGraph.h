#pragma once
#include <vector>
#include <functional>
using std::size_t;
struct IGraph {
  virtual ~IGraph() = default;

  virtual void AddEdge(size_t from, size_t to) = 0;

  [[nodiscard]] virtual size_t VerticesCount() const = 0;

  [[nodiscard]] virtual std::vector<size_t> GetNextVertices(size_t vertex) const = 0;

  [[nodiscard]] virtual std::vector<size_t> GetPrevVertices(size_t vertex) const = 0;
};

void BFS(const IGraph &graph, size_t vertex, const std::function<void(size_t)> &visit);