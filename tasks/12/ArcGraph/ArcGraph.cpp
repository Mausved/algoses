#include <cassert>
#include "ArcGraph.h"

ArcGraph::ArcGraph(size_t vertexCount) :
	vertexCount(vertexCount) {
}

ArcGraph::ArcGraph(const IGraph &graph) :
	vertexCount(graph.VerticesCount()) {
  for (size_t i{0}; i < vertexCount; ++i) {
	for (auto &next : graph.GetNextVertices(i)) {
	  pairsList.emplace_back(i, next);
	}
  }
}

void ArcGraph::AddEdge(size_t from, size_t to) {
  assert(from >= 0 && from < vertexCount);
  assert(to >= 0 && to < vertexCount);
  pairsList.emplace_back(from, to);
}

size_t ArcGraph::VerticesCount() const { return vertexCount; }

std::vector<size_t> ArcGraph::GetNextVertices(size_t vertex) const {
  std::vector<size_t> vertices(0);
  for (auto &pair : pairsList) {
	if (pair.first == vertex)
	  vertices.push_back(pair.second);
  }
  return vertices;
}

std::vector<size_t> ArcGraph::GetPrevVertices(size_t vertex) const {
  std::vector<size_t> vertices(0);
  for (auto &pair : pairsList) {
	if (pair.second == vertex)
	  vertices.push_back(pair.first);
  }
  return vertices;
}
