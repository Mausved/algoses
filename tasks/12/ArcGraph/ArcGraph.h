#pragma once

#include "IGraph.h"

class ArcGraph : public IGraph {
 public:
  explicit ArcGraph(size_t vertexCount);

  explicit ArcGraph(const IGraph &graph);

  void AddEdge(size_t from, size_t to) final;

  [[nodiscard]] size_t VerticesCount() const final;

  [[nodiscard]] std::vector <size_t> GetNextVertices(size_t vertex) const final;

  [[nodiscard]] std::vector <size_t> GetPrevVertices(size_t vertex) const final;

 private:
  size_t vertexCount;
  std::vector <std::pair<size_t, size_t>> pairsList;

};
