#pragma once
#include "IGraph.h"

class ListGraph : public IGraph {
 public:
  explicit ListGraph(size_t vertexCount);

  explicit ListGraph(const IGraph &_graph);

  void AddEdge(size_t from, size_t to) final;

  [[nodiscard]] size_t VerticesCount() const final;

  [[nodiscard]] std::vector <size_t> GetNextVertices(size_t vertex) const final;

  [[nodiscard]] std::vector <size_t> GetPrevVertices(size_t inputVertex) const final;

 private:
  std::vector <std::vector<size_t>> lists;
};