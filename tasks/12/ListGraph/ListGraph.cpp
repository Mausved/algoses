#include "ListGraph.h"
#include <cassert>

ListGraph::ListGraph(size_t vertexCount) : lists(vertexCount) {}

ListGraph::ListGraph(const IGraph &_graph) : lists(_graph.VerticesCount()) {
  for (size_t i{0}; i < lists.size(); i++) {
	lists[i] = _graph.GetNextVertices(i);
  }
}

void ListGraph::AddEdge(size_t from, size_t to) {
  assert(from >= 0 && from < lists.size());
  assert(to >= 0 && to < lists.size());
  lists[from].push_back(to);
}

size_t ListGraph::VerticesCount() const { return lists.size(); }

std::vector<size_t> ListGraph::GetNextVertices(size_t vertex) const {
  assert(vertex >= 0 && vertex < lists.size());
  return lists[vertex];
}

std::vector<size_t> ListGraph::GetPrevVertices(size_t inputVertex) const {
  assert(inputVertex >= 0 && inputVertex < lists.size());
  std::vector<size_t> prev_vertices;
  for (size_t i{0}; i < lists.size(); i++) {
	for (size_t j{0}; j < lists[i].size(); j++) {
	  if (inputVertex == lists[i][j]) {
		prev_vertices.push_back(i);
	  }
	}
  }
  return prev_vertices;
}