#pragma once

#include "IGraph.h"
#include "unordered_set"
#include <set>
class SetGraph : public IGraph {
 public:
  explicit SetGraph(size_t vertexCount);

  explicit SetGraph(const IGraph &graph);

  void AddEdge(size_t from, size_t to) final;

  [[nodiscard]] size_t VerticesCount() const final;

  [[nodiscard]] std::vector<size_t> GetNextVertices(size_t vertex) const final;

  [[nodiscard]] std::vector<size_t> GetPrevVertices(size_t vertex) const final;

 private:
  std::vector<std::set<size_t>> vectorSet;
};

