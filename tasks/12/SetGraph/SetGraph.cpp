#include <cassert>
#include "SetGraph.h"
SetGraph::SetGraph(size_t vertexCount) :
	vectorSet(vertexCount) {}

SetGraph::SetGraph(const IGraph &graph) :
	vectorSet(graph.VerticesCount(), std::set<size_t>()) {
  for (size_t i{0}; i < graph.VerticesCount(); i++) {
	for (auto &vertex : graph.GetNextVertices(i)) {
	  vectorSet[i].insert(vertex);
	}
  }
}

void SetGraph::AddEdge(size_t from, size_t to) {
  assert(from >= 0 && from < vectorSet.size());
  assert(to >= 0 && to < vectorSet.size());
  vectorSet[from].insert(to);
}

size_t SetGraph::VerticesCount() const {
  return vectorSet.size();
}

std::vector<size_t> SetGraph::GetNextVertices(size_t vertex) const {
  assert(vertex >= 0 && vertex < vectorSet.size());
  return {vectorSet[vertex].begin(), vectorSet[vertex].end()};
}

std::vector<size_t> SetGraph::GetPrevVertices(size_t vertex) const {
  assert(vertex >= 0 && vertex < vectorSet.size());
  std::vector<size_t> vertices(0);
  for (size_t i{0}; i < vectorSet.size(); ++i) {
	if (vectorSet[i].find(vertex) != vectorSet[i].end()) {
	  vertices.push_back(i);
	}
  }
  return vertices;
}
