#pragma once

#include "IGraph.h"

class MatrixGraph : public IGraph {
 public:
  explicit MatrixGraph(size_t vertexCount);

  explicit MatrixGraph(const IGraph &graph);

  void AddEdge(size_t from, size_t to) final;

  [[nodiscard]] size_t VerticesCount() const final;

  [[nodiscard]] std::vector <size_t> GetNextVertices(size_t vertex) const final;

  [[nodiscard]] std::vector <size_t> GetPrevVertices(size_t vertex) const final;

 private:
  std::vector <std::vector<bool>> matrix;

};
