#include <cassert>
#include "MatrixGraph.h"

MatrixGraph::MatrixGraph(size_t vertexCount) :
	matrix(vertexCount, std::vector<bool>(vertexCount, false)) {}

MatrixGraph::MatrixGraph(const IGraph &graph) :
	matrix(graph.VerticesCount(), std::vector<bool>(graph.VerticesCount(), false)) {
  for (size_t i{0}; i < graph.VerticesCount(); i++) {
	for (auto &vertex : graph.GetNextVertices(i))
	  matrix[i][vertex] = true;
  }
}

void MatrixGraph::AddEdge(size_t from, size_t to) {
  assert(from >= 0 && from < matrix.size());
  assert(to >= 0 && to < matrix.size());
  matrix[from][to] = true;
}

size_t MatrixGraph::VerticesCount() const {
  return matrix.size();
}

std::vector<size_t> MatrixGraph::GetNextVertices(size_t vertex) const {
  assert(vertex >= 0 && vertex < matrix.size());
  std::vector<size_t> vertices;
  for (size_t i{0}; i < matrix.size(); i++) {
	if (matrix[vertex][i])
	  vertices.push_back(i);
  }
  return vertices;
}

std::vector<size_t> MatrixGraph::GetPrevVertices(size_t vertex) const {
  assert(vertex >= 0 && vertex < matrix.size());
  std::vector<size_t> vertices;
  for (size_t i{0}; i < matrix.size(); i++) {
	if (matrix[i][vertex])
	  vertices.push_back(i);
  }
  return vertices;
}
