#include <iostream>
#include "ListGraph/ListGraph.h"
#include "SetGraph/SetGraph.h"
#include "MatrixGraph/MatrixGraph.h"
#include "ArcGraph/ArcGraph.h"

void run(std::ostream& out = std::cout, std::istream& in = std::cin) {
    auto printFunc([&out](size_t vertex) { out << vertex << ' ';});

    ListGraph listGraph(6);
    listGraph.AddEdge(0, 1);
    listGraph.AddEdge(0, 2);
    listGraph.AddEdge(0, 3);
    listGraph.AddEdge(1, 3);
    listGraph.AddEdge(2, 3);
    listGraph.AddEdge(2, 4);
    listGraph.AddEdge(4, 5);

    std::cout << "ListGraph: ";
    BFS(listGraph, 0, printFunc);
    std::cout << std::endl;
    ArcGraph arcGraph(listGraph);
    arcGraph.AddEdge(1, 2);
    MatrixGraph matrixGraph(arcGraph);
    matrixGraph.AddEdge(1, 4);
    SetGraph setGraph(matrixGraph);
    setGraph.AddEdge(3, 4);
    ListGraph newListGraph(setGraph);

    std::cout << "arcGraph: ";
    BFS(arcGraph, 0, printFunc);
    std::cout << std::endl << "matrixGraph: ";
    BFS(matrixGraph, 0, printFunc);
    std::cout << std::endl << "setGraph: ";
    BFS(setGraph, 0, printFunc);
    std::cout << std::endl << "newsListGraph: ";
    BFS(newListGraph, 0, printFunc);
}


int main() {
    run();
    return 0;
}
