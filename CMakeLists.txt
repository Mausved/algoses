cmake_minimum_required(VERSION 3.5)

project(algoses)
enable_language(CXX)
set(CMAKE_CXX_STANDARD 17)

#set(CMAKE_CXX_FLAGS "\
#    ${CMAKE_CXX_FLAGS} -Wall -Werror -Wpedantic \
#    -Wfloat-conversion -Wvla \
#    -Wno-unknown-pragmas \
#    -Wno-sign-compare -Woverloaded-virtual \
#    -Wwrite-strings -Wno-unused \
#    -Wfloat-equal -pthread -Wpedantic -Wextra")
#set(CMAKE_CXX_CLANG_TIDY clang-tidy)
#set(CMAKE_CXX_CPPCHECK cppcheck ./, --error-exitcode=1)
#set(CMAKE_CXX_CPPLINT cpplint)
add_executable(1_1 tasks/1_1.cpp)
add_executable(2_2 tasks/2_2.cpp)
add_executable(3_2 tasks/3_2.cpp)
add_executable(4_3 tasks/4_3.cpp)
add_executable(5_4 tasks/5_4.cpp)
add_executable(6_4 tasks/6_4.cpp)
add_executable(7_3 tasks/7_3.cpp)
add_executable(8_1 tasks/8_1.cpp)
add_executable(9_2 tasks/9_2.cpp)
add_executable(10 tasks/10.cpp)
add_executable(11_2 tasks/11_2.cpp)
add_executable(13 tasks/13.cpp)
add_executable(14 tasks/14.cpp)


